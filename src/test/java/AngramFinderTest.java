import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AngramFinderTest {

    @Test
    public void checkIfTwoStringsAreAnagram() {
        char str1[] = {'t', 'e', 's', 't'};
        char str2[] = {'t', 's', 'e', 't'};
        assertTrue(AnagramFinder.areAnagram(str1, str2));

    }


}
