import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class AnagramFinder {

    public static void main(String[] arg) {
        List<String> words = FileReader.readWordsFromFile();

        words.stream().map(s -> words.stream().filter(
                s1 -> areAnagram(s.toCharArray(), s1.toCharArray()))
                .collect(joining(" ")))
                .collect(Collectors.filtering(s -> s.trim().contains(" "), Collectors.toSet()))
                .forEach(System.out::println);

    }

    static boolean areAnagram(char[] str1, char[] str2) {
        int n1 = str1.length;
        int n2 = str2.length;


        if (n1 != n2)
            return false;

        // Sort both strings
        Arrays.sort(str1);
        Arrays.sort(str2);

        // Compare sorted strings
        for (int i = 0; i < n1; i++)
            if (str1[i] != str2[i])
                return false;
        return true;
    }


}
