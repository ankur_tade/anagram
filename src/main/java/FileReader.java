import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class FileReader {

    static final String FILE_PATH = "eventyr.txt";

    static List<String> readWordsFromFile() {
        try {
            return  Files.readAllLines(Paths.get(Thread.currentThread()
                    .getContextClassLoader().getResource(FILE_PATH).toURI()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
